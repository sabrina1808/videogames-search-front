import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../components/Home.vue';
import Favorite from '../components/Favorite.vue';

Vue.use(VueRouter);

//Création des diverses routes de l'application
const routes = [
    {path: '/', component: Home},
    {path: '/Favorite', component: Favorite}
];

//On exporte le router
export const router = new VueRouter({
    routes: routes
});