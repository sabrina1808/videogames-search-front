import Vue from 'vue'
import App from './App.vue'
import { router } from './helper/router'
import * as VueGoogleMaps from 'vue2-google-maps';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrashAlt, faHome, faHeart, faClock, faRoute } from '@fortawesome/free-solid-svg-icons'

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: "Your API KEY",
    libraries: "places, directions",
    v: 3.41
  }
});

library.add(faTrashAlt, faHome, faHeart, faClock, faRoute);

Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
