export const choices = [
    {val: "micromania", name: "Micromania"},
    {val: "maxxi games", name: "Maxxi Games"},
    {val: "carrefour", name: "Carrefour"},
    {val: "auchan", name: "Auchan"},
    {val: "fnac", name: "Fnac"},
    {val: "cultura", name: "Cultura"},
    {val: "album", name: "Album"},
    {val: "album bd", name: "Album BD"},
    {val: 'boulanger', name: "Boulanger"},
    {val: "game cash", name: "Game Cash"},
    {val: "trader games", name: "Trader Games"},
    {val: "retrogame", name: "Retrogame"},
    {val: "hobby one", name:"Hobby One"},
    {val: "steeve console", name: "Steeve Console"},
    {val: "japan arcade", name: "Japan Arcade"},
    {val: "hayaku shop manga", name: "Hayaku Shop Manga"}
];